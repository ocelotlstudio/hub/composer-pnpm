# syntax=docker/dockerfile:1
FROM php:8.1.15RC1-zts-bullseye
SHELL ["/bin/bash", "--login", "-c"]
RUN apt update && apt upgrade -y
RUN apt install curl git zip libzip-dev libicu-dev libxml2-dev -y
COPY --from=composer:latest /usr/bin/composer /usr/local/bin/composer
RUN docker-php-ext-install mysqli && docker-php-ext-enable mysqli
RUN docker-php-ext-install zip && docker-php-ext-enable zip
RUN docker-php-ext-install intl && docker-php-ext-enable intl
RUN docker-php-ext-install soap && docker-php-ext-enable soap
RUN curl -fsSL https://get.pnpm.io/install.sh | bash -
RUN cp $(which pnpm) /usr/local/bin/pnpm && chmod +x /usr/local/bin/pnpm
RUN pnpm env use --global lts
RUN cp $(which node) /usr/local/bin/node && chmod +x /usr/local/bin/node
RUN pnpm config set store-dir .pnpm-store
